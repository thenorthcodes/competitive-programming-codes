/*Birthday Paradox Solution Code*/

//1st January 2018

/* Find minimum number of people required such that
 * the probability of two people having same birthday 
 * is at least p
 * (Assume Non-Leap Year)
 */
 
/* Solution: p = 1 - q 
 * where q is the probability of two people having different birthdays
 * q =  365/365 * 364/365 * 363/365 *..... * 365-(n-1)/365
 * i.e. first person can have any of the 365 out of 365 days as his birthday
 * second person can have any of the remaining 364 days for a diff birthday
 * and so on, nth person can have any of the remaining 365-(n-1) remaining days
 * therefore, if say p >= 50%, => 1 - q >= 50% => q <= 50%
 * similarly, if p >= 90% , q <= 10%
 */


#include <iostream>
using namespace std;
int main() {
    
    float num = 365;
    float denum = 365;
    int n = 0; //number of people with different birthdays
    float q = 1;
    
    while( q > 0.1 ) //i.e. p should be at least 90%
    {
        q = q * (num)/(denum);
        n++;
        num--;
        cout<< q << " "<< n <<endl ;
    }
    
    cout<< "\n Required Min. Probability  - "<<(1-q)*100;
    cout<< "\n Number of People - "<<n;
    
    return 0;
}