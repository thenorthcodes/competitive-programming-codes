/* GERMAN LOTTO */

//1st January 2018

/* From a given set of numbers, select 6 in ascending order */

/* Solution: Brute force, six nested loops
 * Complexity: O(n^6)
 * Assume array sorted
 */

#include <iostream>
using namespace std;
int main() {
    int a[] = {1,3,5,7,9,12,14,16,18,20};
    int n = sizeof(a)/sizeof(int);
    
    for(int i=0; i<n-5; i++){
        for(int j=i+1; j<n-4; j++){
            for(int k=j+1; k<n-3;k++){
                for(int l=k+1; l<n-2; l++){
                    for(int m=l+1; m<n-1; m++){
                        for(int o=m+1; o<n; o++){
                            cout<<a[i]<<" "<<a[i]<<" "<<a[j]<<" "<<a[k]<<" "<<a[l]<<" "<<a[m]<<" "<<a[o]<<endl;
                        }
                    }
                }
            }
        }
    }
    return 0;
}
