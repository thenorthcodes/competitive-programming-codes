/*
https://leetcode.com/problems/maximum-subarray/

Tags: Easy,  Array, Top 100 Interview Questions

Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.

Example:

Input: [-2,1,-3,4,-1,2,1,-5,4],
Output: 6
Explanation: [4,-1,2,1] has the largest sum = 6.

Test Case Input: [-1]
Output: -1

Test Case Input: [-2,-1]
Output: -1

Follow up:

If you have figured out the O(n) solution, try coding another solution using the divide and conquer approach, which is more subtle. - ??
*/

//O(n) Solution

class Solution {
public:
    int maxSubArray(vector<int>& nums) {
        //if(nums.size()==1) return nums[0];
        int currsum = 0, maxsum = INT_MIN, maxelem = nums[0];
        for(int i=0; i<nums.size(); i++){
            currsum += nums[i];
            if(currsum<0) currsum = 0;
            maxsum = max(currsum, maxsum);
            maxelem = max(nums[i], maxelem);
            // cout<<maxsum<<endl;
        }
        if(maxsum<=0 && maxelem<0) return maxelem;
        return maxsum;
    }
};