/*
https://leetcode.com/problems/majority-element/

Tags: Easy,  Array, Top 100 Interview Questions

Given an array of size n, find the majority element. The majority element is the element that appears more than ⌊ n/2 ⌋ times.

You may assume that the array is non-empty and the majority element always exist in the array.

Example 1:

Input: [3,2,3]
Output: 3
Example 2:

Input: [2,2,1,1,1,2,2]
Output: 2

*/

// O(n log n) Solution

class Solution {
public:
    int majorityElement(vector<int>& nums) {
        sort(nums.begin(),nums.end());
        int count = 0;
        for(int i=0; i<nums.size()-1; i++){
            count++;
            if(count>nums.size()/2) return nums[i];
            if(nums[i]!=nums[i+1]){
                count=0;
            }
        }
        if(nums[nums.size()-1]==nums[nums.size()-2]) count++;
        else count = 1;
        return nums[nums.size()-1];
}
    
};

