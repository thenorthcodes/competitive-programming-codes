/*
https://leetcode.com/problems/missing-number/

Tags: Easy,  Array, Top 100 Interview Questions

Given an array containing n distinct numbers taken from 0, 1, 2, ..., n, find the one that is missing from the array.

Example 1:

Input: [3,0,1]
Output: 2
Example 2:

Input: [9,6,4,2,3,5,7,0,1]
Output: 8
Note:
Your algorithm should run in linear runtime complexity. Could you implement it using only constant extra space complexit

*/

// Hash Table: O(n) solution
// Sorting O(n log n)
// Bit Manip O(n)
// Sum of first n numbers O(n)

// O(n) Solution

class Solution {
public:
    int missingNumber(vector<int>& nums) {
        int arr[1000000] = {0};
        for(int i=0; i<nums.size(); i++){
            arr[nums[i]] = 1;            
        }
        int i;
        for(i=0; i<=nums.size(); i++){
            if(arr[i]==0){
                return i;                
            }            
        }
        return i;
    }
};
